<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#424242" />
    <?php
        $titleresult = $this->customlib->getTitleName();
        if (!empty($titleresult["name"])) {
            $title_name = $titleresult["name"];
        } else {
            $title_name = "Hospital Name Title";
        }
    ?>
    <title><?php echo $title_name; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--favican-->
    <link href="<?php echo base_url(); ?>backend/images/s-favican.png" rel="shortcut icon" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/usertemplate/temp/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/usertemplate/temp/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/usertemplate/temp/css/icon-font.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/usertemplate/temp/css/animate.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/usertemplate/temp/css/animsition.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/usertemplate/temp/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/usertemplate/temp/css/main.css">

</head>

<body>
    <div class="limiter">
        <div class="container-login100"
            style="background-image: url('<?php echo base_url(); ?>backend/usertemplate/temp/bg.jpg');">
            <div class="wrap-login100 p-t-30 p-b-50">
                <?php
                    $logoresult = $this->customlib->getLogoImage();
                    if (!empty($logoresult["image"])) {
                        $logo_image = base_url() . "uploads/hospital_content/logo/" . $logoresult["image"];
                    } else {
                        $logo_image = base_url() . "uploads/hospital_content/logo/s_logo.png";
                    }
                    if (!empty($logoresult["mini_logo"])) {
                        $mini_logo = base_url() . "uploads/hospital_content/logo/" . $logoresult["mini_logo"];
                    } else {
                        $mini_logo = base_url() . "uploads/hospital_content/logo/smalllogo.png";
                    }
                ?>
                <span class="login100-form-title p-b-41">
                    <img src="<?php echo $logo_image; ?>" style="height: 80px; margin-bottom: 10px;"> 
                    <br>
                    <?php echo $this->lang->line('forgot_password'); ?>
                </span>
                <?php
                    if (isset($error_message)) {
                        echo "<div class='alert alert-danger'>" . $error_message . "</div>";
                    }
                    ?>
                    <?php
                    if ($this->session->flashdata('message')) {
                        echo "<div class='alert alert-success'>" . $this->session->flashdata('message') . "</div>";
                    };
                ?>
                <form action="<?php echo site_url('site/ufpassword') ?>" method="post" class="login100-form validate-form p-b-33 p-t-5">
                    <?php echo $this->customlib->getCSRF(); ?>
                    <div class="wrap-input100 validate-input">
                        <input class="input100" type="text" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" placeholder="<?php echo $this->lang->line('email'); ?>" autocomplete="off">
                        <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                        <span class="text-danger"><?php echo form_error('username'); ?></span>
                        <input  name="user" type="hidden" value="patient">
                    </div>
                    <div class="container-login100-form-btn m-t-32">
                        <button type="submit" class="login100-form-btn">
                            <?php echo $this->lang->line('submit'); ?>
                        </button>
                    </div>
                </form>
                <p style="color: #fff;"><a href="<?php echo site_url('site/userlogin') ?>" style="color: #fff;" class="forgot"><i class="fa fa-key"></i> <?php echo $this->lang->line('user_login'); ?></a> </p> 

            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>backend/usertemplate/temp/js/jquery-3.2.1.min.js"></script>

    <script src="<?php echo base_url(); ?>backend/usertemplate/temp/js/animsition.min.js"></script>

    <script src="<?php echo base_url(); ?>backend/usertemplate/temp/js/popper.js"></script>
    <script src="<?php echo base_url(); ?>backend/usertemplate/temp/js/bootstrap.min.js"></script>


</body>

</html>