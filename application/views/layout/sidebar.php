<aside class="main-sidebar" id="alert2">
    <?php if ($this->rbac->hasPrivilege('student', 'can_view')) {?>
        <form class="navbar-form navbar-left search-form2" role="search"  action="<?php echo site_url('admin/admin/search'); ?>" method="POST">
            <?php echo $this->customlib->getCSRF(); ?>
            <div class="input-group ">
                <input type="text"  name="search_text" class="form-control search-form" placeholder="<?php echo $this->lang->line('search_by_name'); ?>">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" style="padding: 3px 12px !important;border-radius: 0px 30px 30px 0px; background: #fff;" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
    <?php }?>
    <section class="sidebar" id="sibe-box">
        <?php $this->load->view('layout/top_sidemenu');?>
        <ul class="sidebar-menu verttop">
            <?php
if ($this->module_lib->hasActive('front_office')) {
    if ($this->rbac->hasPrivilege('appointment', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('front_office'); ?>">
                        <a  href="<?php echo base_url(); ?>admin/appointment/search">
                            <i class="material-icons">home</i> <span><?php echo $this->lang->line('front_office'); ?></span>
                        </a>
                    </li>
                    <?php
}
}
?>
                    <?php
if ($this->module_lib->hasActive('OPD')) {
    if ($this->rbac->hasPrivilege('opd_patient', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('OPD_Out_Patient'); ?>">
                        <a href="<?php echo base_url(); ?>admin/patient/search">
                            <i class="material-icons">account_circle</i> <span> <?php echo $this->lang->line('opd_out_patient'); ?></span>
                        </a>
                    </li>
                <?php }
}
?>
            <?php
if ($this->module_lib->hasActive('IPD')) {
    if ($this->rbac->hasPrivilege('ipd_patient', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('IPD_in_patient'); ?>">
                        <a href="<?php echo base_url() ?>admin/patient/ipdsearch">
                            <i class="material-icons" aria-hidden="true">table_chart</i> <span> <?php echo $this->lang->line('ipd_in_patient'); ?></span>
                        </a>
                    </li>
                <?php }
}
?>
            <?php
if ($this->module_lib->hasActive('pharmacy')) {
    if ($this->rbac->hasPrivilege('pharmacy bill', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('pharmacy'); ?>">
                        <a href="<?php echo base_url(); ?>admin/pharmacy/bill">
                            <i class="material-icons">description</i> <span> <?php echo $this->lang->line('pharmacy'); ?></span>
                        </a>
                    </li>
                <?php }
}
?>
            <?php
if ($this->module_lib->hasActive('pathology')) {
    if ($this->rbac->hasPrivilege('pathology test', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('pathology'); ?>">
                        <a href="<?php echo base_url(); ?>admin/pathology/search">
                            <i class="material-icons">date_range</i> <span><?php echo $this->lang->line('pathology'); ?></span>
                        </a>
                    </li>
                <?php }
}
?>
    <?php
if ($this->module_lib->hasActive('radiology')) {
    if ($this->rbac->hasPrivilege('radiology test', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('radiology'); ?>">
                        <a href="<?php echo base_url(); ?>admin/radio/search">
                            <i class="material-icons">account_balance</i> <span><?php echo $this->lang->line('radiology'); ?></span>
                        </a>
                    </li>
                <?php }
}
?>
<?php
if ($this->module_lib->hasActive('operation_theatre')) {
    if ($this->rbac->hasPrivilege('ot_patient', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('operation_theatre'); ?> ">
                        <a href="<?php echo base_url() ?>admin/operationtheatre/otsearch">
                            <i class="material-icons">gavel</i> <span><?php echo $this->lang->line('operation_theatre'); ?></span>
                        </a>
                    </li>
                <?php }
}
?>
<?php
if ($this->module_lib->hasActive('blood_bank')) {
    if ($this->rbac->hasPrivilege('blood_bank_status', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('blood_bank'); ?>">
                        <a href="<?php echo base_url() ?>admin/bloodbankstatus/">
                            <i class="material-icons">support</i> <span><?php echo $this->lang->line('blood_bank'); ?></span>
                        </a>
                    </li>
    <?php }
}
?>
 <?php if ($this->module_lib->hasActive('zoom_live_meeting')) {
    if (($this->rbac->hasPrivilege('live_consultation', 'can_view')) || ($this->rbac->hasPrivilege('live_meeting', 'can_view'))) {?>
                      <li class="treeview <?php echo set_Topmenu('conference'); ?>">
                       <a href="#">
                            <i class="fa fa-video-camera ftlayer"></i> <span><?php echo $this->lang->line('live_consult'); ?></span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                         <ul class="treeview-menu">
                            <?php if ($this->rbac->hasPrivilege('live_consultation', 'can_view')) {?>
                                <li class="<?php echo set_Submenu('conference/live_consult'); ?>"><a href="<?php echo base_url('admin/conference/consult'); ?>"><i class="material-icons">table_view</i> <?php echo $this->lang->line('live_consult'); ?></a></li>
                            <?php }if ($this->rbac->hasPrivilege('live_meeting', 'can_view')) {?>
                                <li class="<?php echo set_Submenu('conference/live_meeting'); ?>"><a href="<?php echo base_url('admin/conference/meeting'); ?>"><i class="material-icons">redeem</i> <?php echo $this->lang->line('live_meeting'); ?> </a></li>
                            <?php }?>
                        </ul>
                    </li>
             <?php
}
}?>
<?php
if ($this->module_lib->hasActive('tpa_management')) {
    if ($this->rbac->hasPrivilege('organisation', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('tpa_management'); ?>">
                        <a href="<?php echo base_url() ?>admin/tpamanagement">
                            <i class="material-icons">ads_click</i> <span><?php echo $this->lang->line('tpa_management'); ?></span>
                        </a>
                    </li>
        <?php
}
}
if (($this->module_lib->hasActive('income')) || ($this->module_lib->hasActive('expense'))) {
    if (($this->rbac->hasPrivilege('income', 'can_view')) || ($this->rbac->hasPrivilege('expense', 'can_view'))) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('finance'); ?>">
                        <a href="<?php echo base_url(); ?>admin/patient/search">
                            <i class="material-icons">speaker_notes</i> <span><?php echo $this->lang->line('finance'); ?></span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <?php
if ($this->module_lib->hasActive('income')) {
            if ($this->rbac->hasPrivilege('income', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('income/index'); ?>"><a href="<?php echo base_url(); ?>admin/income"><i class="material-icons">segment</i> <?php echo $this->lang->line('income'); ?> </a></li>
                                    <?php
}
        }
        if ($this->module_lib->hasActive('expense')) {
            if ($this->rbac->hasPrivilege('expense', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('expense/index'); ?>"><a href="<?php echo base_url(); ?>admin/expense"><i class="material-icons">tour</i> <?php echo $this->lang->line('expenses'); ?></a></li>
                        <?php }
        }
        ?>
                        </ul>
                    </li>
        <?php
}
}
if ($this->module_lib->hasActive('ambulance')) {
    if ($this->rbac->hasPrivilege('ambulance', 'can_view')) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('Transport'); ?>">
                        <a href="<?php echo base_url(); ?>admin/vehicle/search">
                            <i class="material-icons" aria-hidden="true"></i>
                            <span> <?php echo $this->lang->line('ambulance'); ?></span>
                        </a>
                    </li>
        <?php
}
}
if (($this->module_lib->hasActive('birth_death_report')) || ($this->module_lib->hasActive('birth_death_report'))) {
    if (($this->rbac->hasPrivilege('birth_record', 'can_view')) || ($this->rbac->hasPrivilege('death_record', 'can_view'))) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('birthordeath'); ?>">
                        <a href="<?php echo base_url(); ?>admin/birthordeath"><i class="fa fa-birthday-cake" aria-hidden="true"></i><span> <?php echo $this->lang->line('birth_death_record ') . " " . $this->lang->line('birth_death_record'); ?></span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <?php
if ($this->module_lib->hasActive('birth_death_report')) {
            if ($this->rbac->hasPrivilege('birth_record', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('birthordeath/index'); ?>"><a href="<?php echo base_url(); ?>admin/birthordeath"><i class="material-icons">settings_ethernet</i> <?php echo $this->lang->line('birth_record'); ?> </a></li>
                <?php
}
        }

        if ($this->rbac->hasPrivilege('death_record', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('birthordeath/death'); ?>"><a href="<?php echo base_url(); ?>admin/birthordeath/death"><i class="material-icons">toll</i> <?php echo $this->lang->line('death_record'); ?></a></li>
                    <?php }?>
                        </ul>
                    </li>
                    <?php
}
}

if ($this->module_lib->hasActive('human_resource')) {
    if (($this->rbac->hasPrivilege('staff', 'can_view') ||
        $this->rbac->hasPrivilege('staff_attendance', 'can_view') ||
        $this->rbac->hasPrivilege('staff_attendance_report', 'can_view') ||
        $this->rbac->hasPrivilege('staff_payroll', 'can_view') ||
        $this->rbac->hasPrivilege('payroll_report', 'can_view'))) {
        ?>

                    <li class="treeview <?php echo set_Topmenu('HR'); ?>">
                        <a href="<?php echo base_url(); ?>admin/staff">
                            <i class="material-icons">all_inbox</i> <span><?php echo $this->lang->line('human_resource'); ?></span>
                        </a>
                    </li>
                    <?php
}
}

if ($this->module_lib->hasActive('communicate')) {
    if (($this->rbac->hasPrivilege('notice_board', 'can_view') ||
        $this->rbac->hasPrivilege('email_sms', 'can_view') ||
        $this->rbac->hasPrivilege('email_sms_log', 'can_view'))) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('Messaging'); ?>">
                        <a href= "<?php echo base_url(); ?>admin/notification">
                            <i class = "fas fa-bullhorn"></i> <span><?php echo $this->lang->line('messaging'); ?></span>
                        </a>
                    </li>
        <?php
}
}
if ($this->module_lib->hasActive('download_center')) {
    if (($this->rbac->hasPrivilege('upload_content', 'can_view'))) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('Download Center'); ?>">
                        <a href="<?php echo base_url(); ?>admin/content">
                            <i class="material-icons">settings_input_component</i> <span><?php echo $this->lang->line('download_center'); ?></span>
                        </a>
                    </li>
                    <?php
}
}

if ($this->module_lib->hasActive('inventory')) {
    if (($this->rbac->hasPrivilege('issue_item', 'can_view') ||
        $this->rbac->hasPrivilege('item_stock', 'can_view') ||
        $this->rbac->hasPrivilege('item', 'can_view') ||
        $this->rbac->hasPrivilege('item_category', 'can_view') ||
        $this->rbac->hasPrivilege('item_category', 'can_view') ||
        $this->rbac->hasPrivilege('store', 'can_view') ||
        $this->rbac->hasPrivilege('supplier', 'can_view'))) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('Inventory'); ?>">
                        <a href="<?php echo base_url(); ?>admin/itemstock">
                            <i class="material-icons">online_prediction</i> <span><?php echo $this->lang->line('inventory'); ?></span>
                        </a>
                    </li>
                    <?php
}
}
if ($this->module_lib->hasActive('front_cms')) {
    if (($this->rbac->hasPrivilege('event', 'can_view') ||
        $this->rbac->hasPrivilege('gallery', 'can_view') ||
        $this->rbac->hasPrivilege('notice', 'can_view') ||
        $this->rbac->hasPrivilege('media_manager', 'can_view') ||
        $this->rbac->hasPrivilege('pages', 'can_view') ||
        $this->rbac->hasPrivilege('menus', 'can_view') ||
        $this->rbac->hasPrivilege('banner_images', 'can_view'))) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('Front CMS'); ?>">
                        <a href="<?php echo base_url(); ?>admin/front/page">
                            <i class="material-icons">view_sidebar</i> <span><?php echo $this->lang->line('front_cms'); ?></span>
                        </a>
                    </li>
        <?php
}
}

if ($this->module_lib->hasActive('reports')) {
    if (($this->rbac->hasPrivilege('transaction_report', 'can_view')) || ($this->rbac->hasPrivilege('appointment_report', 'can_view')) || ($this->rbac->hasPrivilege('opd_report', 'can_view')) || ($this->rbac->hasPrivilege('ipd_report', 'can_view')) || ($this->rbac->hasPrivilege('discharge_patient_report', 'can_view')) || ($this->rbac->hasPrivilege('pharmacy_bill_report', 'can_view')) || ($this->rbac->hasPrivilege('pathology_patient_report', 'can_view')) || ($this->rbac->hasPrivilege('radiology_patient_report', 'can_view')) || ($this->rbac->hasPrivilege('ot_report', 'can_view')) || ($this->rbac->hasPrivilege('blood_issue_report', 'can_view')) || ($this->rbac->hasPrivilege('tpa_report', 'can_view')) || ($this->rbac->hasPrivilege('income_report', 'can_view')) || ($this->rbac->hasPrivilege('expense_report', 'can_view')) || ($this->rbac->hasPrivilege('ambulance_report', 'can_view')) || ($this->rbac->hasPrivilege('payroll_month_report', 'can_view')) || ($this->rbac->hasPrivilege('payroll_report', 'can_view')) || ($this->rbac->hasPrivilege('staff_attendance_report', 'can_view')) || ($this->rbac->hasPrivilege('transaction_report', 'can_view')) || ($this->rbac->hasPrivilege('user_log', 'can_view')) || ($this->rbac->hasPrivilege('patient_login_credential', 'can_view')) || ($this->rbac->hasPrivilege('email_sms_log', 'can_view')) || ($this->rbac->hasPrivilege('income_group_report', 'can_view')) || ($this->rbac->hasPrivilege('expense_group_report', 'can_view')) || ($this->rbac->hasPrivilege('inventory_stock_report', 'can_view')) || ($this->rbac->hasPrivilege('add_item_report', 'can_view')) || ($this->rbac->hasPrivilege('issue_inventory_report', 'can_view')) || ($this->rbac->hasPrivilege('opd_balance_report', 'can_view')) || ($this->rbac->hasPrivilege('ipd_balance_report', 'can_view'))) {
        ?>
                    <li class="treeview <?php echo set_Topmenu('Reports'); ?>">
                        <a href="#">
                            <i class="material-icons">app_blocking</i> <span><?php echo $this->lang->line('reports'); ?></span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <?php if ($this->rbac->hasPrivilege('transaction_report', 'can_view')) {?>
                                <li class="<?php echo set_Submenu('admin/income/transactionreport'); ?>"><a href="<?php echo base_url(); ?>admin/income/transactionreport"><i class="material-icons">view_day</i> <?php echo $this->lang->line('transaction_report'); ?></a>
                                </li>
                                <?php
}
        if ($this->module_lib->hasActive('front_office')) {
            if ($this->rbac->hasPrivilege('appointment_report', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('admin/appointment/appointmentReport'); ?>"><a href="<?php echo base_url(); ?>admin/appointment/appointmentReport"><i class="material-icons">picture_in_picture</i> <?php echo $this->lang->line('appointment') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('OPD')) {
            if ($this->rbac->hasPrivilege('opd_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/patient/opd_report'); ?>"><a href="<?php echo base_url(); ?>admin/patient/opd_report"><i class="material-icons">home_work</i> <?php echo $this->lang->line('opd') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('IPD')) {
            if ($this->rbac->hasPrivilege('ipd_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/patient/ipdreport'); ?>"><a href="<?php echo base_url(); ?>admin/patient/ipdreport"><i class="material-icons">content_cut</i> <?php echo $this->lang->line('ipd') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('OPD')) {
            if ($this->rbac->hasPrivilege('opd_balance_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/patient/opdreportbalance'); ?>"><a href="<?php echo base_url(); ?>admin/patient/opdreportbalance"><i class="material-icons">stream</i> <?php echo $this->lang->line('opd') . " " . $this->lang->line('balance') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('IPD')) {
            if ($this->rbac->hasPrivilege('ipd_balance_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/patient/ipdreportbalance'); ?>"><a href="<?php echo base_url(); ?>admin/patient/ipdreportbalance"><i class="material-icons">weekend</i> <?php echo $this->lang->line('ipd') . " " . $this->lang->line('balance') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('IPD')) {
            if ($this->rbac->hasPrivilege('discharge_patient_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/patient/dischargepatientReport'); ?>"><a href="<?php echo base_url(); ?>admin/patient/dischargepatientReport"><i class="material-icons">how_to_vote</i> <?php echo $this->lang->line('discharged') . " " . $this->lang->line('patient'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('pharmacy')) {
            if ($this->rbac->hasPrivilege('pharmacy_bill_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/pharmacy/billreport'); ?>"><a href="<?php echo base_url(); ?>admin/pharmacy/billreport"><i class="material-icons">gesture</i> <?php echo $this->lang->line('pharmacy') . " " . $this->lang->line('bill') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('pharmacy')) {
            if ($this->rbac->hasPrivilege('expiry_medicine_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/expmedicine/expmedicinereport'); ?>"><a href="<?php echo base_url(); ?>admin/expmedicine/expmedicinereport"><i class="material-icons">precision_manufacturing</i> <?php echo $this->lang->line('expmedicine') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('pathology')) {
            if ($this->rbac->hasPrivilege('pathology_patient_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/pathology/pathologyreport'); ?>"><a href="<?php echo base_url(); ?>admin/pathology/pathologyreport"><i class="material-icons">coronavirus</i> <?php echo $this->lang->line('pathology') . " " . $this->lang->line('patient') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }
        if ($this->module_lib->hasActive('radiology')) {
            if ($this->rbac->hasPrivilege('radiology_patient_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/radio/radiologyreport'); ?>"><a href="<?php echo base_url(); ?>admin/radio/radiologyreport"><i class="material-icons">whatshot</i> <?php echo $this->lang->line('radiology') . " " . $this->lang->line('patient') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }
        if ($this->module_lib->hasActive('operation_theatre')) {
            if ($this->rbac->hasPrivilege('ot_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/operationtheatre/otreport'); ?>"><a href="<?php echo base_url(); ?>admin/operationtheatre/otreport"><i class="material-icons">outdoor_grill</i> <?php echo $this->lang->line('ot') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('blood_bank')) {
            if ($this->rbac->hasPrivilege('blood_issue_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/bloodbank/bloodissuereport'); ?>"><a href="<?php echo base_url(); ?>admin/bloodbank/bloodissuereport"><i class="material-icons">clean_hands</i> <?php echo $this->lang->line('blood') . " " . $this->lang->line('issue') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('blood_bank')) {
            if ($this->rbac->hasPrivilege('blood_donor_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/bloodbank/blooddonorreport'); ?>"><a href="<?php echo base_url(); ?>admin/bloodbank/blooddonorreport"><i class="material-icons">personal_injury</i> <?php echo $this->lang->line('blood') . " " . $this->lang->line('donor') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('zoom_live_meeting')) {
            if ($this->rbac->hasPrivilege('live_consultation_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('conference/consult_report'); ?>"><a href="<?php echo base_url('admin/conference/consult_report'); ?>"><i class="material-icons">sports_mma</i> <?php echo $this->lang->line('live_consult') . ' ' . $this->lang->line('report'); ?></a></li>
                                            <?php
}
        }

        if ($this->module_lib->hasActive('zoom_live_meeting')) {
            if ($this->rbac->hasPrivilege('live_meeting_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('conference/meeting_report'); ?>"><a href="<?php echo base_url('admin/conference/meeting_report'); ?>"><i class="material-icons">vaccines</i> <?php echo $this->lang->line('live_meeting') . ' ' . $this->lang->line('report'); ?></a></li>
                                            <?php
}
        }

        if ($this->module_lib->hasActive('tpa_management')) {
            if ($this->rbac->hasPrivilege('tpa_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/tpamanagement/tpareport'); ?>"><a href="<?php echo base_url(); ?>admin/tpamanagement/tpareport"><i class="material-icons">south_america</i> <?php echo $this->lang->line('organisation') . " " . $this->lang->line('report'); ?></a></li>
                                    <?php
}
        }
        if ($this->module_lib->hasActive('income')) {
            if ($this->rbac->hasPrivilege('income_report', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('admin/income/incomesearch'); ?>"><a href="<?php echo base_url(); ?>admin/income/incomesearch"><i class="material-icons">sports_hockey</i> <?php echo $this->lang->line('income') . " " . $this->lang->line('report'); ?></a></li>
                                    <?php
}
        }

        if ($this->module_lib->hasActive('income')) {
            if ($this->rbac->hasPrivilege('income_group_report', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('reports/incomegroup'); ?>"><a href="<?php echo base_url(); ?>admin/income/incomegroup"><i class="material-icons">tune</i> <?php echo $this->lang->line('income') . ' ' . $this->lang->line('group') . ' ' . $this->lang->line('report'); ?></a></li>
                                    <?php
}
        }

        if ($this->module_lib->hasActive('expense')) {
            if ($this->rbac->hasPrivilege('expense_report', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('admin/expense/expensesearch'); ?>"><a href="<?php echo base_url(); ?>admin/expense/expensesearch"><i class="material-icons">wb_sunny</i> <?php echo $this->lang->line('expense') . " " . $this->lang->line('report'); ?></a></li>
                                    <?php
}
        }

        if ($this->module_lib->hasActive('expense')) {
            if ($this->rbac->hasPrivilege('expense_group_report', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('reports/expensegroup'); ?>"><a href="<?php echo base_url(); ?>admin/expense/expensegroup"><i class="material-icons">timelapse</i> <?php echo $this->lang->line('expense') . ' ' . $this->lang->line('group') . ' ' . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }

        if ($this->module_lib->hasActive('ambulance')) {
            if ($this->rbac->hasPrivilege('ambulance_report', 'can_view')) {
                ?><li class="<?php echo set_Submenu('admin/vehicle/ambulancereport'); ?>"><a href="<?php echo base_url(); ?>admin/vehicle/ambulancereport"><i class="material-icons">compare</i> <?php echo $this->lang->line('ambulance') . " " . $this->lang->line('report'); ?></a></li>
                                        <?php
}
        }
    if ($this->module_lib->hasActive('birth_death_report')) {
        if ($this->rbac->hasPrivilege('birth_report', 'can_view')) {
            ?><li class="<?php echo set_Submenu('admin/birthordeath/birthreport'); ?>"><a href="<?php echo base_url(); ?>admin/birthordeath/birthreport"><i class="material-icons">animation</i> <?php echo $this->lang->line('birth') . " " . $this->lang->line('report'); ?></a></li>
                                    <?php
}
}
    if ($this->module_lib->hasActive('birth_death_report')) {
        if ($this->rbac->hasPrivilege('death_report', 'can_view')) {
            ?><li class="<?php echo set_Submenu('admin/birthordeath/deathreport'); ?>"><a href="<?php echo base_url(); ?>admin/birthordeath/deathreport"><i class="material-icons">motion_photos_on</i> <?php echo $this->lang->line('death') . " " . $this->lang->line('report'); ?></a></li>
                                <?php
}
}
        if ($this->rbac->hasPrivilege('payroll_month_report', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('admin/payroll/payrollreport'); ?>"><a href="<?php echo base_url(); ?>admin/payroll/payrollreport"><i class="material-icons">thermostat_auto</i> <?php echo $this->lang->line('payroll') . " " . $this->lang->line('month') . " " . $this->lang->line('report'); ?></a></li>
                                <?php
}
        if ($this->rbac->hasPrivilege('payroll_report', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('admin/payroll/payrollsearch'); ?>"><a href="<?php echo base_url(); ?>admin/payroll/payrollsearch"><i class="material-icons">crop_rotate</i> <?php echo $this->lang->line('payroll_report'); ?></a></li>
                                <?php
}
        if ($this->rbac->hasPrivilege('staff_attendance_report', 'can_view')) {
            ?>

                                <li class="<?php echo set_Submenu('admin/staffattendance/attendancereport'); ?>"><a href="<?php echo base_url(); ?>admin/staffattendance/attendancereport"><i class="material-icons">monochrome_photos</i> <?php echo $this->lang->line('staff_attendance_report'); ?></a></li>
                                <?php
}

        if ($this->rbac->hasPrivilege('user_log', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('userlog/index'); ?>"><a href="<?php echo base_url(); ?>admin/userlog"><i class="material-icons">panorama_photosphere</i> <?php echo $this->lang->line('user_log'); ?></a></li>
                                <?php
}
        if ($this->rbac->hasPrivilege('patient_login_credential', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('admin/patient/patientcredentialreport'); ?>"><a href="<?php echo base_url(); ?>admin/patient/patientcredentialreport"><i class="material-icons">image_aspect_ratio</i> <?php echo $this->lang->line('patient_login_credential'); ?></a></li>
                                <?php
}
        if ($this->module_lib->hasActive('communicate')) {
            if ($this->rbac->hasPrivilege('email_sms_log', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('mailsms/index'); ?>"><a href="<?php echo base_url(); ?>admin/mailsms/index"><i class="material-icons">document_scanner</i> <?php echo $this->lang->line('email_/_sms_log'); ?></a></li>
                                    <?php
}
        }
        if ($this->module_lib->hasActive('inventory')) {
            if ($this->rbac->hasPrivilege('inventory_stock_report', 'can_view')) {
                ?>

                                    <li class="<?php echo set_Submenu('Reports/itemreport'); ?>"><a href="<?php echo base_url(); ?>admin/item/itemreport"><i class="material-icons">phonelink_ring</i> <?php echo $this->lang->line('inventory') . ' ' . $this->lang->line('stock') . ' ' . $this->lang->line('report'); ?></a></li>
                                    <?php
}
        }
        if ($this->module_lib->hasActive('inventory')) {
            if ($this->rbac->hasPrivilege('add_item_report', 'can_view')) {
                ?>

                                    <li class="<?php echo set_Submenu('Reports/additemreport'); ?>"><a href="<?php echo base_url(); ?>admin/item/additemreport"><i class="material-icons">ring_volume</i> <?php echo $this->lang->line('inventory') . ' ' . $this->lang->line('item') . ' ' . $this->lang->line('report'); ?></a></li>
                                    <?php
}
        }
        if ($this->module_lib->hasActive('inventory')) {
            if ($this->rbac->hasPrivilege('issue_inventory_report', 'can_view')) {
                ?>
                                    <li class="<?php echo set_Submenu('Reports/issueinventoryreport'); ?>"><a href="<?php echo base_url(); ?>admin/issueitem/issueinventoryreport"><i class="material-icons">grid_view</i> <?php echo $this->lang->line('inventory') . ' ' . $this->lang->line('issue') . ' ' . $this->lang->line('report'); ?></a></li>
                        <?php }
        }
        ?>

                        </ul>
                    </li>
                            <?php
}
}
if (($this->rbac->hasPrivilege('general_setting', 'can_view')) || ($this->rbac->hasPrivilege('charges', 'can_view')) || ($this->rbac->hasPrivilege('bed_status', 'can_view')) || ($this->rbac->hasPrivilege('opd_prescription_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('ipd_prescription_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('pharmacy_bill_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('setup_front_office', 'can_view')) || ($this->rbac->hasPrivilege('medicine_category', 'can_view')) || ($this->rbac->hasPrivilege('pathology_category', 'can_view')) || ($this->rbac->hasPrivilege('radiology_category', 'can_view')) || ($this->rbac->hasPrivilege('income_head', 'can_view')) || $this->rbac->hasPrivilege('leave_types', 'can_view') || ($this->rbac->hasPrivilege('item_category', 'can_view')) || ($this->rbac->hasPrivilege('hospital_charges', 'can_view')) || ($this->rbac->hasPrivilege('medicine_supplier', 'can_view')) || ($this->rbac->hasPrivilege('medicine_dosage', 'can_view'))) {
    ?>
                <li class="treeview <?php echo set_Topmenu('setup'); ?>">
                    <a href="<?php echo base_url(); ?>">
                        <i class="material-icons">workspaces</i> <span><?php echo $this->lang->line('setup'); ?></span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
if ($this->rbac->hasPrivilege('general_setting', 'can_view')) {
        ?>
                            <li class="<?php echo set_Submenu('schsettings/index'); ?>"><a href="<?php echo base_url(); ?>schsettings"><i class="material-icons">restaurant_menu</i> <?php echo $this->lang->line('settings'); ?></a></li>
                            <?php
}
    if ($this->rbac->hasPrivilege('patient', 'can_view')) {
        ?>

                            <li class="<?php echo set_Submenu('setup/patient'); ?>"> <a href="<?php echo base_url(); ?>admin/admin/search"><i class="material-icons">delivery_dining</i> <?php echo $this->lang->line('patient'); ?></a></li>
                            <?php
}

    if ($this->rbac->hasPrivilege('hospital_charges', 'can_view')) {
        ?>
                            <li class="<?php echo set_Submenu('charges/index'); ?>"><a href="<?php echo base_url(); ?>admin/charges"><i class="material-icons">fastfood</i> <?php echo $this->lang->line('hospital') . " " . $this->lang->line('charges'); ?></a></li>
                            <?php
}
    if ($this->module_lib->hasActive('IPD')) {
        if ($this->rbac->hasPrivilege('bed', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('bed'); ?>"><a href="<?php echo base_url(); ?>admin/setup/bed/status"><i class="material-icons">local_florist</i> <?php echo $this->lang->line('bed'); ?></a></li>
                                <?php
}
    }

    if (($this->rbac->hasPrivilege('opd_prescription_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('ipd_bill_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('ipd_prescription_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('pharmacy_bill_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('print_payslip_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('birth_print_header_footer', 'can_view')) || ($this->rbac->hasPrivilege('death_print_header_footer', 'can_view'))) {
        ?>
                            <li class="<?php echo set_Submenu('admin/printing'); ?>"><a href="<?php echo base_url(); ?>admin/printing"><i class="material-icons">dinner_dining</i> <?php echo $this->lang->line('print') . " " . $this->lang->line('header') . " " . $this->lang->line('footer'); ?></a></li>
                            <?php
}
    if ($this->module_lib->hasActive('front_office')) {
        if ($this->rbac->hasPrivilege('setup_front_office', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('admin/visitorspurpose'); ?>"><a href="<?php echo base_url(); ?>admin/visitorspurpose"><i class="material-icons">emergency</i> <?php echo $this->lang->line('front_office'); ?></a></li>
                                <?php
}
    }
    if ($this->module_lib->hasActive('pharmacy')) {
        if (($this->rbac->hasPrivilege('medicine_category', 'can_view') || ($this->rbac->hasPrivilege('medicine_supplier', 'can_view')) || ($this->rbac->hasPrivilege('medicine_dosage', 'can_view')))) {
            ?>
                                <li class="<?php echo set_Submenu('medicine/index'); ?>"><a href="<?php echo base_url(); ?>admin/medicinecategory/index"><i class="material-icons">airline_stops</i> <?php echo $this->lang->line('pharmacy'); ?></a></li>
                                <?php
}
    }
    if ($this->module_lib->hasActive('pathology')) {
        if ($this->rbac->hasPrivilege('pathology_category', 'can_view') || $this->rbac->hasPrivilege('pathology_unit', 'can_view') || $this->rbac->hasPrivilege('pathology_parameter', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('addCategory/index'); ?>"><a href="<?php echo base_url(); ?>admin/pathologycategory/addcategory"><i class="material-icons">airlines</i> <?php echo $this->lang->line('pathology'); ?></a></li>
                                <?php
}
    }
    if ($this->module_lib->hasActive('radiology')) {
        if ($this->rbac->hasPrivilege('radiology category', 'can_view') || $this->rbac->hasPrivilege('radiology_unit', 'can_view') || $this->rbac->hasPrivilege('radiology_parameter', 'can_view')) {
            ?>
                                <li class="<?php echo set_Submenu('addlab/index'); ?>"><a href="<?php echo base_url(); ?>admin/lab/addLab"><i class="material-icons">games</i> <?php echo $this->lang->line('radiology'); ?></a></li>
                                <?php
}
    }

    if (($this->rbac->hasPrivilege('symptoms_type', 'can_view')) || ($this->rbac->hasPrivilege('symptoms_head', 'can_view'))) {
        ?>
                                <li class="<?php echo set_Submenu('symptoms/index'); ?>"><a href="<?php echo base_url(); ?>admin/symptoms"><i class="material-icons">repeat_on</i> <?php echo $this->lang->line('symptoms'); ?></a></li>
                                <?php
}

    if ($this->rbac->hasPrivilege('setting', 'can_view')) {?>
                                <li class="<?php echo set_Submenu('conference/zoom_api_setting'); ?>"><a href="<?php echo base_url('admin/conference'); ?>"><i class="material-icons">featured_video</i> <?php echo $this->lang->line('zoom_setting') ?></a></li>
                        <?php }

    if (($this->module_lib->hasActive('income')) || ($this->module_lib->hasActive('expense'))) {

        if (($this->rbac->hasPrivilege('income_head', 'can_view')) || ($this->rbac->hasPrivilege('income_head', 'can_view'))) {
            ?>
                                <li class="<?php echo set_Submenu('finance/index'); ?>"><a href="<?php echo base_url(); ?>admin/incomehead"><i class="material-icons">radio_button_checked</i> <?php echo $this->lang->line('finance'); ?></a></li>
                                <?php
}
    }
    if ($this->rbac->hasPrivilege('birth_death_customfields', 'can_view')) {
        ?>
                            <li class="<?php echo set_Submenu('birthordeathcustom/index'); ?>"> <a href="<?php echo base_url(); ?>admin/birthordeathcustom"><i class="material-icons">attach_file</i> <?php echo $this->lang->line('birth_death_record'); ?></a></li>
                            <?php
}
    if (($this->rbac->hasPrivilege('leave_types', 'can_view')) || ($this->rbac->hasPrivilege('leave_types', 'can_view'))) {
        ?>
                            <li class="<?php echo set_Submenu('hr/index'); ?>"><a href="<?php echo base_url(); ?>admin/leavetypes"><i class="material-icons">query_stats</i> <?php echo $this->lang->line('human_resource'); ?></a></li>
                            <?php
}
    if ($this->module_lib->hasActive('inventory')) {
        if ($this->rbac->hasPrivilege('item_category', 'can_view')) {
            ?>
                                        <li class="<?php echo set_Submenu('inventory/index'); ?>"><a href="<?php echo base_url(); ?>admin/itemcategory"><i class="material-icons">schema</i> <?php echo $this->lang->line('inventory'); ?></a></li>
                            <?php }
    }
    ?>

                            </ul>

                        </li>
                        <?php
}

if ($this->module_lib->hasActive('system_settings')) {
    if (($this->rbac->hasPrivilege('general_setting', 'can_view') ||
        $this->rbac->hasPrivilege('session_setting', 'can_view') ||
        $this->rbac->hasPrivilege('notification_setting', 'can_view') ||
        $this->rbac->hasPrivilege('sms_setting', 'can_view') ||
        $this->rbac->hasPrivilege('email_setting', 'can_view') ||
        $this->rbac->hasPrivilege('payment_methods', 'can_view') ||
        $this->rbac->hasPrivilege('languages', 'can_view') ||
        $this->rbac->hasPrivilege('languages', 'can_add') ||
        $this->rbac->hasPrivilege('backup_restore', 'can_view') ||
        $this->rbac->hasPrivilege('front_cms_setting', 'can_view'))) {
        ?>

        <?php
}
}
?>

        </ul>
    </section>
</aside>
